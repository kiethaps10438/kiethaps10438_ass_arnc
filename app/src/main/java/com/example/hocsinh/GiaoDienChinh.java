package com.example.hocsinh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GiaoDienChinh extends AppCompatActivity {
    Button btnsv, btnmap, btnnews, btnface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_chinh);
        btnsv = findViewById(R.id.btnsv);
        btnmap = findViewById(R.id.btnmap);
        btnnews = findViewById(R.id.btnnews);
        btnface = findViewById(R.id.btnface);


        btnmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GiaoDienChinh.this,MAP.class);
                startActivity(intent);

            }
        });
        btnnews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GiaoDienChinh.this,NEWS.class);
                startActivity(intent);
            }
        });
        btnface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GiaoDienChinh.this,FaceBook.class);
                startActivity(intent);
            }
        });
        btnsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GiaoDienChinh.this,QLSV.class);
                startActivity(intent);
            }
        });

    }
}
