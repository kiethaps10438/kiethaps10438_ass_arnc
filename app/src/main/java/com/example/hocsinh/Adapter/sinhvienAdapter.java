package com.example.hocsinh.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hocsinh.Model.sinhvien;
import com.example.hocsinh.R;
import com.example.hocsinh.Model.sinhvien;
import com.example.hocsinh.R;

import java.util.ArrayList;

public class sinhvienAdapter extends BaseAdapter {
    Context context;
    ArrayList<sinhvien> dulieusv;

    public sinhvienAdapter(Context context, ArrayList<sinhvien> dulieusv) {
        this.context = context;
        this.dulieusv = dulieusv;
    }

    @Override
    public int getCount() {
        return dulieusv.size();
    }

    @Override
    public Object getItem(int position) {
        return dulieusv.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertViewsv, ViewGroup parent) {

        if (convertViewsv==null){
            LayoutInflater layoutInflatersv=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// cho get co 2 lua chon
            convertViewsv=layoutInflatersv.inflate(R.layout.item_sv,null);

        }

        // anh xa
        TextView tvsttsv=convertViewsv.findViewById(R.id.tvsttsv);
        TextView tvmasv=convertViewsv.findViewById(R.id.tvmasv);
        TextView tvmalopsv=convertViewsv.findViewById(R.id.tvmalopsv);
        TextView tvtensv=convertViewsv.findViewById(R.id.tvtensv);
        TextView tvngaysinh=convertViewsv.findViewById(R.id.tvngaysinh);
        if (position%2==0){

            convertViewsv.setBackgroundColor(Color.GREEN);
            tvsttsv.setTextColor(Color.YELLOW);
            tvmasv.setTextColor(Color.YELLOW);
            tvmalopsv.setTextColor(Color.YELLOW);
            tvtensv.setTextColor(Color.YELLOW);
            tvngaysinh.setTextColor(Color.YELLOW);
        }

//gan  du lieu
        sinhvien w=dulieusv.get(position);

        tvsttsv.setText(position+1+"");
        tvmasv.setText(w.getMasv());
        tvmalopsv.setText(w.getLop());
        tvtensv.setText(w.getTensv());
        tvngaysinh.setText(w.getNgaysv());



        return convertViewsv;
    }
}
