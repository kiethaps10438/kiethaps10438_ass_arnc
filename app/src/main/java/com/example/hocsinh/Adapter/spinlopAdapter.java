package com.example.hocsinh.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.Model.lop;
import com.example.hocsinh.R;

import java.util.ArrayList;

public class spinlopAdapter extends BaseAdapter {
    Context context;
    ArrayList<lop> dulieu;
    lopDAO lDAO;
    public spinlopAdapter(Context context, ArrayList<lop> dulieu) {
        this.context = context;
        this.dulieu = dulieu;
        lDAO=new lopDAO(context);
    }

    @Override
    public int getCount() {
        return dulieu.size();
    }

    @Override
    public Object getItem(int position) {
        return dulieu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// cho get co 2 lua chon
            convertView=layoutInflater.inflate(R.layout.spinlop,null);
        }

        TextView tvspinmalop=convertView.findViewById(R.id.tvspinmalop);


        tvspinmalop.setText(dulieu.get(position).getMalop());
        // bo sung them cho dep de hieu

        return convertView;
    }
}
