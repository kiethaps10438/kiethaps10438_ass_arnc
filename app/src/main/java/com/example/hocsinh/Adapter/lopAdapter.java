package com.example.hocsinh.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hocsinh.Model.lop;
import com.example.hocsinh.R;
import com.example.hocsinh.Model.lop;
import com.example.hocsinh.R;

import java.util.ArrayList;

public class lopAdapter extends BaseAdapter {
    Context context;
ArrayList<lop> dulieu;

    public lopAdapter(Context context, ArrayList<lop> dulieu) {
        this.context = context;
        this.dulieu = dulieu;
    }

    @Override
    public int getCount() {
        return dulieu.size();
    }

    @Override
    public Object getItem(int position) {
        return dulieu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// cho get co 2 lua chon
            convertView=layoutInflater.inflate(R.layout.item_lop,null);

        }

        // anh xa
TextView tvsttlop=convertView.findViewById(R.id.tvsttlop);
        TextView tvmalop=convertView.findViewById(R.id.tvmalop);
        TextView tvtenlop=convertView.findViewById(R.id.tvtenlop);



//gan  du lieu
        lop w=dulieu.get(position);
tvsttlop.setText(position+1+"");// vi stt la posison ,ma posison bat dau la 0 de +1 cho dep
tvmalop.setText(w.getMalop());
tvtenlop.setText(w.getTenlop());

        return convertView ;
    }
}
