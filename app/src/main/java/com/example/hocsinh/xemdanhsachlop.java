package com.example.hocsinh;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hocsinh.Adapter.lopAdapter;
import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.Model.lop;

import java.util.ArrayList;

public class xemdanhsachlop extends AppCompatActivity {
    lopDAO lDAO;
    ImageButton emgbtnadd,emgbtnrefresh;
    lopAdapter lAdapter;
    ArrayList<lop> list;
    ListView lvdanhsachlop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.danhsachlop);
        lvdanhsachlop=findViewById(R.id.lvdanhsachlop);

        ImageButton emgbtnadd=findViewById(R.id.emgbtnadd);
        ImageButton emgbtnrefresh=findViewById(R.id.emgbtnrefresh);

        lDAO =new lopDAO(this);
        list=new ArrayList<>();
        list=lDAO.LayTatCaLop();
        lAdapter=new lopAdapter(this,list);
     //    click vao danh sach de xoa
        lvdanhsachlop.setAdapter(lAdapter);
lvdanhsachlop.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(xemdanhsachlop.this);
        alertDialogBuilder.setMessage("Moi ban lua chon!");
        alertDialogBuilder.setPositiveButton("XOa", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


             lop  spf=list.get(position);
               //  spf.getSttlop();
                 spf.getMalop();
                 spf.getTenlop();


                if (lDAO.XoaLop(spf) == -1) {
                    Toast.makeText(getApplicationContext(), "Xoa khong thanh cong", Toast.LENGTH_SHORT).show();
                    return;
                }
                // thành coonng , load lại list view
                Toast.makeText(getApplicationContext(), "Xoa thanh cong", Toast.LENGTH_SHORT).show();
               list = lDAO.LayTatCaLop();
             lAdapter = new lopAdapter(xemdanhsachlop.this,list);
                //cập nhật lại listview
                lvdanhsachlop.setAdapter(lAdapter);


            }
        });
        alertDialogBuilder.setNegativeButton("Sua", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //không làm gì
                Intent sua=new Intent(xemdanhsachlop.this,sualop.class);
                startActivity(sua);
            }
        });
        alertDialogBuilder.show();
        return true;
    }
});

// nut them
        emgbtnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add=new Intent(xemdanhsachlop.this,themlop.class);
                startActivity(add);
            }
        });
        emgbtnrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list = lDAO.LayTatCaLop();
                lAdapter = new lopAdapter(xemdanhsachlop.this,list);
                //cập nhật lại listview
                lvdanhsachlop.setAdapter(lAdapter);

            }
        });

    }


}

