package com.example.hocsinh;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class QLSV extends AppCompatActivity {
    Button btnthemlop,btnxemds,btnquanlysv,btnthoat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qlsv);
        anhxa();
        ControlButton();

    }




    private void anhxa() {
        btnthemlop=(Button)findViewById(R.id.btnthemlop);
        btnxemds=(Button)findViewById(R.id.btnxemds);
        btnquanlysv=(Button)findViewById(R.id.btnquanlysv);
        btnthoat=(Button)findViewById(R.id.btnthoat);
    }


    private void ControlButton() {
        btnthoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(QLSV.this,android.R.style.Theme_DeviceDefault_Light_Dialog);
                builder.setTitle("Ban co chac muon thoat khoi app");
                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        onBackPressed();
                    }
                });
                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {

                    }
                });
                builder.show();
            }
        });
        btnthemlop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(QLSV.this,themlop.class);
                startActivity(intent);


            }
        });
        btnxemds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View a) {
                Intent intent=new Intent(QLSV.this,xemdanhsachlop.class);
                startActivity(intent);
            }
        });
        btnquanlysv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(QLSV.this,quanlysinhvien.class);
                startActivity(intent);
            }
        });
    }
}
