package com.example.hocsinh;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hocsinh.Adapter.lopAdapter;
import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.Model.lop;
import com.example.hocsinh.Adapter.lopAdapter;
import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.Model.lop;

import java.util.ArrayList;

public class sualop extends AppCompatActivity implements View.OnClickListener{
    lopDAO lDAO;
    lopAdapter lAdapter;
    ArrayList<lop> list;
    EditText edtsuamalop,edtsuatenlop;
    Button btnsualop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sualop);
        edtsuamalop=findViewById(R.id.edtsuamalop);
        edtsuatenlop=findViewById(R.id.edtsuatenlop);
        btnsualop=findViewById(R.id.btnsualop);

        lDAO =new lopDAO(this);
//text
        list=new ArrayList<>();
        list = lDAO.LayTatCaLop();
        lAdapter=new lopAdapter(this,list);



        btnsualop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.btnsualop:
                lop spf = new lop();
                spf.setMalop(edtsuamalop.getText().toString());
                spf.setTenlop(edtsuatenlop.getText().toString());


                if (lDAO.SuaLop(spf) == -1) {
                    Toast.makeText(getApplicationContext(), "Sua khong thanh cong", Toast.LENGTH_SHORT).show();
                   return;
                }
                // thành coonng , load lại list view
                Toast.makeText(getApplicationContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                list = lDAO.LayTatCaLop();
                lAdapter = new lopAdapter(this,list);

                // khi them xong thi se tu dong xoa trangg
                edtsuamalop.setText("");
                edtsuatenlop.setText("");

                edtsuamalop.requestFocus();
                break;
        }
    }
}
