package com.example.hocsinh;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hocsinh.Adapter.lopAdapter;
import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.Model.lop;

import java.util.ArrayList;

public class themlop extends AppCompatActivity implements View.OnClickListener {
EditText edtthemmalop,edtthemtenlop;
Button btnxoatrang,btnthemlopdia,btnthoatlopdia;
lopDAO lDAO;
    lopAdapter lAdapter;
    ArrayList<lop> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.themlop);
        anhxa();
btnxoatrang.setOnClickListener(this);
        btnthemlopdia.setOnClickListener(this);
        btnthoatlopdia.setOnClickListener(this);

        lDAO =new lopDAO(this);
//text
        list=new ArrayList<>();
        list=lDAO.LayTatCaLop();
        lAdapter=new lopAdapter(this,list);
    }

    private void anhxa() {
        edtthemmalop=findViewById(R.id.edtthemmalop);
        edtthemtenlop=findViewById(R.id.edtthemtenlop);
        btnxoatrang=findViewById(R.id.btnxoatrang);
        btnthemlopdia=findViewById(R.id.btnthemlopdia);
        btnthoatlopdia=findViewById(R.id.btnthoatlopdia);
        // anhxa listview de load lai

    }

    @Override
    public void onClick(View v) {
int id=v.getId();

switch (id){
    case R.id.btnthemlopdia:
        lop spf = new lop();
       spf.setMalop(edtthemmalop.getText().toString());
        spf.setTenlop(edtthemtenlop.getText().toString());

        if (lDAO.ThemLop(spf) == -1) {
            Toast.makeText(getApplicationContext(), "Them khong thanh cong", Toast.LENGTH_SHORT).show();

            return;
        }
        // thành coonng , load lại list view
        Toast.makeText(getApplicationContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
        list = lDAO.LayTatCaLop();
        lAdapter = new lopAdapter(this,list);

        break;
    case R.id.btnxoatrang:
        edtthemmalop.setText("");
        edtthemtenlop.setText("");
        edtthemmalop.requestFocus();
        break;
    case R.id.btnthoatlopdia:
        Intent intent=new Intent(themlop.this,MainActivity.class);
        startActivity(intent);
        break;
}

    }
}
