package com.example.hocsinh;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hocsinh.Adapter.lopAdapter;
import com.example.hocsinh.Adapter.sinhvienAdapter;
import com.example.hocsinh.Adapter.spinlopAdapter;
import com.example.hocsinh.DBhelper.lopDAO;
import com.example.hocsinh.DBhelper.sinhvienDAO;
import com.example.hocsinh.Model.lop;
import com.example.hocsinh.Model.sinhvien;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class quanlysinhvien extends AppCompatActivity implements View.OnClickListener {
ListView lvdanhsachsv;
    Spinner splop;
    EditText edttensv,edtmasv;
TextView edtngaysinh;
    Button btnthemsv,btnsuasv,btnxoasv;
    lopDAO lDAO;
    sinhvienDAO sDAO;
    lopAdapter lAdapter;
    sinhvienAdapter sAdapter;
    ArrayList<lop> danhsachmalop;
    ArrayList<sinhvien> listsv;
    spinlopAdapter spinAdapter;
    //tao ham datepicker



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quanlysinhvien);
        anhxasv();
        btnthemsv.setOnClickListener(this);
        btnsuasv.setOnClickListener(this);
        btnxoasv.setOnClickListener(this);
        // lay spinerlop
        lDAO =new lopDAO(quanlysinhvien.this);
        danhsachmalop=new ArrayList<>();
        danhsachmalop=lDAO.LayMaLop();
    spinAdapter=new spinlopAdapter(this,danhsachmalop);
    splop.setAdapter(spinAdapter);

   splop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        sDAO =new sinhvienDAO(quanlysinhvien.this);
        listsv=new ArrayList<>();

        listsv=sDAO.LaySvTheoLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());

 //  listsv=sDAO.LayTatSinhVien();
        sAdapter=new sinhvienAdapter(quanlysinhvien.this,listsv);
        lvdanhsachsv.setAdapter(sAdapter);

    }

    @Override
  public void onNothingSelected(AdapterView<?> parent) {
    }
    });

// tao datepickerdialog
edtngaysinh.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
ChonNgay();

    }
});



// onlick cho lisview de ghi du lieu len edittext
        lvdanhsachsv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sinhvien s=listsv.get(position);
                edtmasv.setText(s.getMasv());
                edttensv.setText(s.getTensv());
                edtngaysinh.setText(s.getNgaysv());

            }
        });
    }
private void ChonNgay(){
final Calendar calendar= Calendar.getInstance();
int ngay=calendar.get(Calendar.DATE);
    int thang=calendar.get(Calendar.MONTH);
    int nam=calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
calendar.set(year,month,dayOfMonth);
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                edtngaysinh.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
}





// anh xa

    private void anhxasv() {
        splop=(Spinner)findViewById(R.id.splop);
        edtmasv=(EditText)findViewById(R.id.edtmasv);
        edttensv=(EditText)findViewById(R.id.edttensv);
        edtngaysinh=(TextView) findViewById(R.id.edtngaysinh);
        btnthemsv=(Button)findViewById(R.id.btnthemsv);
        btnsuasv=(Button)findViewById(R.id.btnsuasv);
        btnxoasv=(Button)findViewById(R.id.btnxoasv);
        lvdanhsachsv=(ListView)findViewById(R.id.lvdanhsachsv);

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        String tensv=edttensv.getText().toString();
        String masv=edtmasv.getText().toString();
        String ngaysinh=edtngaysinh.getText().toString();
        switch (id){
            case R.id.btnthemsv:
                sinhvien sv = new sinhvien();
             //   try {
                    sv.setLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                    sv.setMasv(edtmasv.getText().toString());
                    sv.setTensv(edttensv.getText().toString());
                    sv.setNgaysv(edtngaysinh.getText().toString());



// neu sDAO==-1 thi them khong thanh cong
                if (sDAO.ThemSV(sv) == -1) {
                    Toast.makeText(getApplicationContext(), "Them khong thanh cong", Toast.LENGTH_SHORT).show();
                    return;
                }
                // thành coonng , load lại list view
                Toast.makeText(getApplicationContext(), "Them thanh cong", Toast.LENGTH_SHORT).show();
               listsv = sDAO.LayTatSinhVien();
                listsv=sDAO.LaySvTheoLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                sAdapter = new sinhvienAdapter(this,listsv);
               lvdanhsachsv.setAdapter(sAdapter);
               // khi them xong thi se tu dong xoa trangg
                edtmasv.setText("");
                edttensv.setText("");
                edtngaysinh.setText("");
                edtmasv.requestFocus();
                break;
                // sua sinh vien

            case R.id.btnsuasv:
                sinhvien sv1 = new sinhvien();

          //      try {
                    sv1.setLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                    sv1.setMasv(edtmasv.getText().toString());
                    sv1.setTensv(edttensv.getText().toString());
                    sv1.setNgaysv(edtngaysinh.getText().toString());


                if (sDAO.SuaSV(sv1) == -1) {
                    Toast.makeText(getApplicationContext(), "Sua khong thanh cong", Toast.LENGTH_SHORT).show();
                    return;
                }
                // thành coonng , load lại list view
                Toast.makeText(getApplicationContext(), "Sua thanh cong", Toast.LENGTH_SHORT).show();
                listsv = sDAO.LayTatSinhVien();
                 listsv=sDAO.LaySvTheoLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                sAdapter = new sinhvienAdapter(this,listsv);
                lvdanhsachsv.setAdapter(sAdapter);
                edtmasv.setText("");
                edttensv.setText("");
                edtngaysinh.setText("");
                edtmasv.requestFocus();
                break;
            case R.id.btnxoasv:
                sinhvien sv2 = new sinhvien();
                    sv2.setLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                    sv2.setMasv(edtmasv.getText().toString());
                    sv2.setTensv(edttensv.getText().toString());
                    sv2.setNgaysv(edtngaysinh.getText().toString());
                if (sDAO.XoaSV(sv2) == -1) {
                    Toast.makeText(getApplicationContext(), "Xoa khong thanh cong", Toast.LENGTH_SHORT).show();
                    return;
                }
                // thành coonng , load lại list view
                Toast.makeText(getApplicationContext(), "Xoa thanh cong", Toast.LENGTH_SHORT).show();
                listsv = sDAO.LayTatSinhVien();
                 listsv=sDAO.LaySvTheoLop(danhsachmalop.get(splop.getSelectedItemPosition()).getMalop());
                sAdapter = new sinhvienAdapter(this,listsv);
                lvdanhsachsv.setAdapter(sAdapter);
                edtmasv.setText("");
                edttensv.setText("");
                edtngaysinh.setText("");
                edtmasv.requestFocus();
                break;
        }

    }
}
