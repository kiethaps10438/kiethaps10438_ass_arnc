package com.example.hocsinh.DBhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class lopDBhelper extends SQLiteOpenHelper {

    public final static String DBSNAME = "QUANLYSANPHAM";
    public final static int DBVERSION = 1;


    public lopDBhelper( Context context) {
        super(context, DBSNAME, null, DBVERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //stt hong can
String lopsql="CREATE TABLE LOP("
        +"MALOP TEXT PRIMARY KEY,"
        +"TENLOP TEXT NOT NULL)";
db.execSQL(lopsql);

    String svsql="CREATE TABLE SV("
            +"MASV TEXT PRIMARY KEY NOT NULL,"
            +"TENSV TEXT NOT NULL,"
            +"NGAYSV TEXT NOT NULL,"
            +"LOPSV TEXT REFERENCES LOP(MALOP))";
       //
    db.execSQL(svsql);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// lop
        String sqllop="DROP TABLE LOP ";
db.execSQL(sqllop);

        String sqlsv="DROP TABLE SV";
        db.execSQL(sqlsv);
onCreate(db);
// sinh vien


    }
}
