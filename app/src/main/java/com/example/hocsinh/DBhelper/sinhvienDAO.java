package com.example.hocsinh.DBhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.hocsinh.Model.sinhvien;

import java.util.ArrayList;

public class sinhvienDAO {
    SQLiteDatabase dp;
    public sinhvienDAO (Context context){
        lopDBhelper sinhviendbHelper = new lopDBhelper(context);
        dp = sinhviendbHelper.getWritableDatabase();
        //sinhviendbHelper.onUpgrade(dp,1,2);

    }

    public ArrayList<sinhvien> LayTatSinhVien(){
        ArrayList<sinhvien> danhsachsv = new ArrayList<>();
        Cursor c = dp.query("SV", null, null, null, null, null, null);
        while (c.moveToNext()) {

            sinhvien S = new sinhvien();
           // try {
                S.setMasv(c.getString(c.getColumnIndex("MASV")));
                S.setTensv(c.getString(c.getColumnIndex("TENSV")));
                S.setNgaysv(c.getString(c.getColumnIndex("NGAYSV")));
                S.setLop(c.getString(c.getColumnIndex("LOPSV")));
                danhsachsv.add(S);

         // } catch (IOException e) {
         //   }

        }
        return danhsachsv;
    }


//lay sv theo lop
    public ArrayList<sinhvien> LaySvTheoLop(String malop){
ArrayList<sinhvien> danhsachsvtheolop = new ArrayList<>();
String sq ="SELECT MASV,TENSV,NGAYSV,LOPSV FROM SV WHERE LOPSV=+'"+malop+"'";
    Cursor c=dp.rawQuery(sq,null);
//c.moveToFirst();
while (c.moveToNext()){
    sinhvien S = new sinhvien();
   // try {
        S.setMasv(c.getString(c.getColumnIndex("MASV")));
        S.setTensv(c.getString(c.getColumnIndex("TENSV")));
        S.setNgaysv(c.getString(c.getColumnIndex("NGAYSV")));
        S.setLop(c.getString(c.getColumnIndex("LOPSV")));
        danhsachsvtheolop.add(S);

   // } catch (IOException e) {
  //  }

}

return danhsachsvtheolop;
    }

  //  private String getDateTime() {
  //      SimpleDateFormat dateFormat = new SimpleDateFormat(
  //             "dd-MM-yyyy");
   //     Date date = new Date();
   //     return dateFormat.format(date);
  //  }

    public long ThemSV(sinhvien sv) {
        ContentValues values = new ContentValues();
        values.put("MASV", sv.getMasv());
        values.put("TENSV", sv.getTensv());
        values.put("NGAYSV", sv.getNgaysv());
       values.put("LOPSV", sv.getLop());
        return dp.insert("SV", null, values);
    }
    public int XoaSV(sinhvien sv){

        return dp.delete("SV","MASV=?",new String[]{sv.getMasv()});
    }

    public int SuaSV(sinhvien sv){

        ContentValues values = new ContentValues();
        values.put("TENSV", sv.getTensv());
        values.put("NGAYSV", sv.getNgaysv());
        values.put("LOPSV", sv.getLop());
        return dp.update("SV",values,"MASV=?",new String[]{sv.getMasv()});

    }
}
