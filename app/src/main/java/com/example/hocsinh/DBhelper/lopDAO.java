package com.example.hocsinh.DBhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.hocsinh.Model.lop;


import java.util.ArrayList;

public class lopDAO {
    SQLiteDatabase dp;
public lopDAO (Context context){
    lopDBhelper lopdbHelper = new lopDBhelper(context);
    dp = lopdbHelper.getWritableDatabase();

}

public ArrayList<lop> LayTatCaLop(){
    ArrayList<lop> danhsachlop = new ArrayList<>();
    Cursor c = dp.query("LOP", null, null, null, null, null, null);
    while (c.moveToNext()) {

        lop L = new lop();
        L.setMalop(c.getString(c.getColumnIndex("MALOP")));
        L.setTenlop(c.getString(c.getColumnIndex("TENLOP")));
        danhsachlop.add(L);

    }
    return danhsachlop;
}

// tao ham lay ma lop
public ArrayList<lop> LayMaLop(){
    ArrayList<lop> danhsachmalop = new ArrayList<>();// noi chua du lieu
    // tao 1 cau truy van
    String sql="SELECT MALOP FROM LOP";
    //rawquery dang tho ket hop voi o tren se lay ra truc tiep
    Cursor c=dp.rawQuery(sql,null);
 //   c.moveToFirst();
    while (c.moveToNext()){
        lop L = new lop();
        L.setMalop(c.getString(c.getColumnIndex("MALOP")));
        danhsachmalop.add(L);

    }

    return danhsachmalop;

}



    public long ThemLop(lop l) {
        ContentValues values = new ContentValues();
        values.put("MALOP", l.getMalop());
        values.put("TENLOP", l.getTenlop());
        return dp.insert("LOP", null, values);
    }
    public int XoaLop(lop l){

        return dp.delete("LOP","MALOP=?",new String[]{l.getMalop()});
    }

    public int SuaLop(lop l){

        ContentValues values = new ContentValues();
        values.put("TENLOP", l.getTenlop());
        return dp.update("LOP",values,"MALOP=?",new String[]{l.getMalop()});

    }
}

